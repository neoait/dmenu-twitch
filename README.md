# dmenu-twitch
Check who is live on twitch via dmenu

## Installation
relies on `python`, `dmenu` and `streamlink` for the heavy
lifting, so make sure these programs are installed.

There is an AUR package available: [dmenu-twitch-git](https://aur.archlinux.org/packages/dmenu-twitch-git/)

## Usage
```
Usage:
     twitch [ OPTION [...] ]

Options:
     -a:        Adds name to the list
     -e:        Opens the list in editor
     -t:        Shows the top streamers
     -l:        Show the list that will be checked
     -f:        Force update
     -h:        Show help
```
