# twitch - dmenu prompt for twitch
# See LICENSE file for copyright and license details.

VERSION = 1.0
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

install:
	mkdir -p ${DESTDIR}${PREFIX}/bin
	install -D twitch ${DESTDIR}${PREFIX}/bin/twitch
	install -D twitch-get ${DESTDIR}${PREFIX}/bin/twitch-get
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < twitch.1 > ${DESTDIR}${MANPREFIX}/man1/twitch.1
	chmod 644 ${DESTDIR}${MANPREFIX}/man1/twitch.1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/twitch\
		${DESTDIR}${PREFIX}/bin/twitch-get\
		${DESTDIR}${MANPREFIX}/man1/twitch.1
